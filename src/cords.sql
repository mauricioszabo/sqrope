DROP TABLE rope;
CREATE TABLE rope(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  left INTEGER,
  right INTEGER,
  minimum_version INTEGER,
  maximum_version INTEGER,
  char_count INTEGER,
  text VARCHAR
);

DELETE FROM rope;
INSERT INTO rope(id, left, right, minimum_version, maximum_version, char_count, text)
VALUES
  (1,   NULL, NULL, 0, NULL, 5,   'Hello'),
  (2,   NULL, NULL, 1, NULL, 7,   ', World'),
  (3,   1,    2,    1, 1,    12,  NULL),
  (5,   1,    6,    2, NULL, 11,  NULL),
  (6,   7,    8,    2, NULL, 6,   NULL),
  (7,   NULL, NULL, 2, NULL, 2,   'Ma'),
  (8,   NULL, NULL, 2, NULL, 4,   'Baby')
  ;

-- Get all "new" version
WITH RECURSIVE
  v AS (select 1 AS version)
  , root AS (
    SELECT id, MAX(char_count)
    FROM rope
    INNER JOIN v
    WHERE minimum_version >= v.version
      AND ( maximum_version <= v.version OR maximum_version IS NULL )
  )
  , tree AS (
    SELECT left, right, char_count, text, maximum_version, 1.0 ord
    FROM rope
    INNER JOIN root ON root.id = rope.id
      UNION
    SELECT rope.left, rope.right, rope.char_count, rope.text, rope.maximum_version,
      CASE
        WHEN tree.left = rope.id THEN tree.ord / 2
        ELSE tree.ord + 1
      END
    FROM rope
    INNER JOIN tree ON tree.left = rope.id OR tree.right = rope.id
    INNER JOIN v
    WHERE ( rope.maximum_version <= v.version OR rope.maximum_version IS NULL )
  )
  , split_text AS (SELECT * FROM tree WHERE text IS NOT NULL ORDER BY ord)
  SELECT GROUP_CONCAT(text, '') FROM split_text;
