const sqlite3 = require('sqlite3').verbose();;

class SQRope {
  constructor(params) {
    const dbName = params.db
    // TODO: Arbitrary number, check performance on big files
    this.rowChunkChars = params.rowChunkChars || 1000
    this.db = new sqlite3.Database(dbName)
    this.version = 0
  }

  async loadString(string) {
    const fragments = this._splitFragments(string)
    this.version++
    if(fragments.length > 0) {
      await this.query('BEGIN')
      return this._loadFragments(this.version, fragments)
        .then(async ({root}) => {
          await this.query(
            'UPDATE rope SET version=? WHERE id = ?', [this.version, root.id]
          )
          await this.query("COMMIT")
          return this.version
        })
        .catch(e => {
          this.version--
          this.query("ROLLBACK")
          return Promise.reject(e)
        })
    } else {
      await this.query(
        `INSERT INTO rope(version, char_count, row_count, last_col_count) VALUES (?, 0, 0, 0)`,
        [this.version]
      )
      return this.version
    }
  }

  async _loadFragments(version, fragments) {
    const leafs = await this._insertLeafs(fragments)
    let leafsOnDB = [...leafs]
    let branchesOnDB = []

    while(true) {
      const left = leafsOnDB.shift()
      const right = leafsOnDB.shift()

      if(right && left) {
        branchesOnDB.push(await this._insertBranch(version, left, right))
      } else if(branchesOnDB.length > 0) {
        leafsOnDB = branchesOnDB
        if(left) leafsOnDB.push(left)
        branchesOnDB = []
      } else {
        return {
          root: left,
          leafs: leafs
        }
      }
    }
  }

  async _insertBranch(version, left, right) {
    let last_col_count = right.last_col_count
    if(right.row_count === 0) last_col_count += left.last_col_count
    const result = await this.query(
      `INSERT INTO rope(row_count, last_col_count, char_count, left, right)
      VALUES(?, ?, ?, ?, ?) RETURNING id, char_count, row_count, last_col_count`,
      [
        left.row_count + right.row_count,
        last_col_count,
        left.char_count + right.char_count,
        left.id, right.id
      ]
    )
    await this.query(`
      INSERT INTO parent(child_id, parent_id, version)
      VALUES (?, ?, ?), (?, ?, ?)
      `, [left.id, result.id, version, right.id, result.id, this.version]
    )
    return result
  }

  async _insertLeafs(fragments) {
    const numberSize = fragments.length.toString().length
    const leafs = fragments.map((text, i) => {
      const lines = text.split("\n")
      const newLineCount = lines.length - 1
      const charCount = text.length
      const lastColCount = lines[lines.length - 1].length

      return [
        text,
        newLineCount,
        lastColCount,
        text.length
      ]
    })
    let leafsOnDB = []
    while(true) {
      const slice = leafs.splice(0, 3000)
      let sql = '(?,?,?,?),'.repeat(slice.length).replace(/,$/, '')
      sql = `INSERT INTO rope(text, row_count, last_col_count, char_count)
        VALUES ${sql} RETURNING id, row_count, last_col_count, char_count, text`
      const lastInserts = await this.queryAll(sql, slice.flat())
      leafsOnDB.push(...lastInserts)
      if(leafs.length === 0) break;
    }
    return leafsOnDB
  }

  query(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.get(sql, params, (error, val) => {
        if(error) {
          reject(error)
        } else {
          resolve(val)
        }
      })
    })
  }

  async update({nodeIds, newText, version}) {
    const oldVersion = (version || this.version)
    const newVersion = oldVersion + 1
    const fragments = this._splitFragments(newText);
    let replacedNodes
    await this.query('BEGIN')
    if(fragments.length === nodeIds.length) {
      replacedNodes = this._replaceNodes(oldVersion, newVersion, nodeIds, fragments)
    } else if(fragments.length > nodeIds.length) {
      nodeIds = [...nodeIds]
      const oldFragments = fragments.splice(0, nodeIds.length)
      // Generate "new elements"
      let newTree = this._loadFragments(newVersion, fragments)
      // Generate the "last element" and a common root
      // const lastFragToBeReplaced = oldFragments.pop()
      const lastId = nodeIds[nodeIds.length - 1]
      const lastOldParent = this._queryParent(
        this.version, lastId
      )
      replacedNodes = this._replaceNodes(
        oldVersion, newVersion, nodeIds, oldFragments,
        async ({newLeafs, childNodes, replacedNodes}) => {
          newTree = await newTree
          const combinationBranch = await this._insertBranch(
            newVersion, newLeafs[newLeafs.length - 1], newTree.root
          )

          const newChildNode = await this._populateOldElems(
            await lastOldParent, combinationBranch, lastId, replacedNodes
          )
          childNodes.pop()
          childNodes.push(newChildNode)
          newLeafs.push(...newTree.leafs)
        }
      )
      // return {}
    }

    return replacedNodes.then(async ({root, newLeafs}) => {
      await this.query('UPDATE rope SET version=? WHERE id = ?', [newVersion, root.id])
      await this.query('COMMIT')
      this.version = newVersion
      return {version: newVersion, nodes: newLeafs}
    }).catch(error => {
      this.query('ROLLBACK')
      return Promise.reject(error)
    })
  }

  async _replaceNodes(version, newVersion, nodeIds, fragments, adjustment = () => {}) {
    let replacedNodes = {}
    let newLeafs = [], childNodes = []
    nodeIds.forEach((id, idx) => {
      const text = fragments[idx]
      const lines = text.split("\n")
      const newLineCount = lines.length - 1
      const charCount = text.length
      const lastColCount = lines[lines.length - 1].length

      const result =
        this._queryParent(version, id).then(oldChildren => {
          return this.query(`
              INSERT INTO rope(text, row_count, last_col_count, char_count)
              VALUES (?,?,?,?) RETURNING id, row_count, last_col_count, char_count, text
              `, [text, newLineCount, lastColCount, charCount]
          ).then(newLeaf => {
            return { oldChildren, newLeaf }
          })
        })
        newLeafs.push(result.then(r => r.newLeaf))
        childNodes.push(result.then(({newLeaf, oldChildren}) =>
          this._populateOldElems(oldChildren, newLeaf, id, replacedNodes)
        ))
    })

    newLeafs = await Promise.all(newLeafs)
    childNodes = await Promise.all(childNodes)
    await adjustment({newLeafs, childNodes, replacedNodes})

    let root
    while(childNodes.length > 0) {
      let grouped = {}
      childNodes.forEach(n => {
        grouped[n.oldParent] ||= []
        grouped[n.oldParent].push(n)
      })

      childNodes = []
      for(const strId in grouped) {
        const id = parseInt(strId)
        const nodes = grouped[id]
        const oldChildren = await this._queryParent(version, id)

        let newNode
        if(nodes.length === 2) {
          const [left, right] = nodes
          newNode = await this._insertBranch(newVersion, left.left, right.right)
        } else {
          const single = nodes[0]
          newNode = await this._insertBranch(newVersion, single.left, single.right)
        }
        if(oldChildren) {
          childNodes.push(this._populateOldElems(oldChildren, newNode, id, replacedNodes))
        } else {
          root = newNode
        }
      }
    }

    return {root, newLeafs}
  }

  _splitFragments(string) {
    let i = 0
    let fragments = []
    while(true) {
      const fragment = string.slice(i, this.rowChunkChars+i)
      if(fragment === '') break
      fragments.push(fragment)
      i += fragment.length
    }
    return fragments
  }

  _populateOldElems(oldChildren, newChild, oldID, replacedNodes) {
    const isLeft = oldChildren.l_id === oldID
    const leftChild = {
      id: oldChildren.l_id,
      row_count: oldChildren.l_row_count,
      last_col_count: oldChildren.l_last_col_count,
      char_count: oldChildren.l_char_count
    }
    const rightChild = {
      id: oldChildren.r_id,
      row_count: oldChildren.r_row_count,
      last_col_count: oldChildren.r_last_col_count,
      char_count: oldChildren.r_char_count
    }
    replacedNodes[oldID] = newChild
    if(isLeft) {
      const other = replacedNodes[rightChild.id] || rightChild
      return {oldParent: oldChildren.parent_id, left: newChild, right: other}
    } else {
      const other = replacedNodes[leftChild.id] || leftChild
      return {oldParent: oldChildren.parent_id, left: other, right: newChild}
    }
  }

  _queryParent(version, id) {
    return this.query(`
      SELECT
        left.row_count l_row_count, left.last_col_count l_last_col_count, left.char_count l_char_count, left.id l_id,
        right.row_count r_row_count, right.last_col_count r_last_col_count, right.char_count r_char_count, right.id r_id,
        lp.parent_id
      FROM parent lp
      INNER JOIN rope parent ON parent.id = lp.parent_id
      LEFT JOIN rope AS left ON left.id = parent.left
      LEFT JOIN rope AS right ON right.id = parent.right
      WHERE lp.version = ?
      AND lp.child_id = ?
      `, [version, id]
    )
  }

  async getText(version = this.version) {
    const result = await this.query(`
      ${templateToGetSiblings}
      SELECT GROUP_CONCAT(text, '') text FROM full_tree
    `, [version])
    return result.text
  }

  async getNodesInRange({start, end}, version = this.version) {
    const sqlTemplate = `SELECT
        rope.id, rope.text, rope.char_count AS root_chars, rope.right,
        rope.row_count num_rows, rope.last_col_count num_cols,
        lft.id AS left, lft.row_count, lft.last_col_count, lft.char_count
      FROM rope
      LEFT JOIN rope lft ON rope.left = lft.id WHERE`
    let firstNode = await this.query(`${sqlTemplate} rope.version = ?`, version)
    let lastNode = firstNode
    const queryRow = id => this.query(`${sqlTemplate} rope.id = ?`, id)

    let firstRow = start.row
    let firstCol = start.column
    let lastRow = end.row
    let lastCol = end.column

    let finalRow = start.row
    let finalCol = start.column
    let searchFirst = true, searchLast = true

    while(searchFirst || searchLast) {
      if(searchFirst) {
        if(firstRow > firstNode.row_count) {
          firstRow -= firstNode.row_count
          firstNode = queryRow(firstNode.right)
        } else if(firstRow < firstNode.row_count || firstCol < firstNode.last_col_count) {
          firstNode = queryRow(firstNode.left)
        } else /*  right row but higher col */ {
          firstRow -= firstNode.row_count
          firstCol -= firstNode.last_col_count
          firstNode = queryRow(firstNode.right)
        }
      }

      if(searchLast) {
        if(searchFirst && lastRow > lastNode.row_count) {
          lastRow = lastRow - lastNode.row_count
          if(lastNode.right === firstRow.id) lastNode = firstNode
          else lastNode = queryRow(lastNode.right)
        } else if(lastRow < lastNode.row_count || lastCol < lastNode.last_col_count) {
          if(lastNode.left === firstRow.id) lastNode = firstNode
          else lastNode = queryRow(lastNode.left)
        } else /*  right row but higher col */ {
          lastRow = lastRow - lastNode.row_count
          lastCol = lastCol - lastNode.last_col_count
          if(lastNode.right === firstRow.id) lastNode = firstNode
          else lastNode = queryRow(lastNode.right)
        }
      }

      firstNode = await firstNode
      lastNode = await lastNode
      searchFirst = firstNode.left
      searchLast = lastNode.left
    }

    const records = await this._getBetween(version, firstNode, lastNode)
    const startCol = start.column - firstCol
    const startRow = start.row - records[0].row_count
    let endRange = {column: startCol, row: startRow}
    records.forEach(({last_col_count, row_count}) => {
      if(row_count > 0) {
        endRange.row += row_count
        endRange.column = last_col_count
      } else {
        endRange.column += last_col_count
      }
    })

    return {
      range: {
        start: { row: startRow, column: startCol },
        end: endRange
      },
      records: records
    }
  }

  _getBetween(version, firstNode, lastNode) {
    return this.queryAll(`
      ${templateToGetSiblings}
      , first_node AS (select depth FROM full_tree WHERE id = ?)
      , last_node AS (select depth FROM full_tree WHERE id = ?)
      SELECT id, text, row_count, last_col_count
      FROM full_tree
      INNER JOIN first_node
      INNER JOIN last_node
      WHERE full_tree.depth BETWEEN first_node.depth AND last_node.depth
    `, [version, firstNode.id, lastNode.id])
  }

  queryAll(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (error, val) => {
        if(error) {
          reject(error)
        } else {
          resolve(val)
        }
      })
    })
  }
}

function createSQRope(params) {
  const rope = new SQRope(params)
  return rope.query(`
    CREATE TABLE IF NOT EXISTS rope(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      left INTEGER,
      right INTEGER,
      version INTEGER,
      char_count INTEGER NOT NULL,
      row_count INTEGER NOT NULL,
      last_col_count INTEGER NOT NULL,
      text VARCHAR
    )
  `)
  .then(() => rope.query(
    `CREATE TABLE IF NOT EXISTS parent(
      child_id INTEGER,
      version INTEGER,
      parent_id INTEGER
    )`
  ))
  .then(() => rope.query(`CREATE INDEX parent_version ON parent(version)`))
  .then(() => rope.query(`CREATE INDEX version ON rope(version)`))
  // .then(() => rope.query(`CREATE INDEX effective_version ON rope(effective_version)`))
  .then(() => rope)
  .catch(e => {
    rope.db.close()
    return Promise.reject(e)
  })
}

// const templateToGetSiblings = `
//   WITH RECURSIVE
//     root AS (
//       SELECT id
//       FROM rope
//       WHERE rope.version = ?
//     )
//     , tree AS (
//       SELECT rope.id, left, right, text, ord
//       FROM rope
//       INNER JOIN root ON root.id = rope.id
//         UNION
//       SELECT rope.id, rope.left, rope.right, rope.text, rope.ord
//       FROM rope
//       INNER JOIN tree ON tree.left = rope.id OR tree.right = rope.id
//     )
//     , full_tree AS (SELECT * FROM tree ORDER BY ord)`

const templateToGetSiblings = `
  WITH RECURSIVE
    root AS (
      SELECT id
      FROM rope
      WHERE rope.version = ?
    )
    , tree AS (
      SELECT rope.id, left, text, right, rope.row_count, rope.last_col_count, '' depth
      FROM rope
      INNER JOIN root ON root.id = rope.id
        UNION
      SELECT rope.id, rope.left, rope.text, rope.right, rope.row_count, rope.last_col_count, tree.depth || '1'
      FROM rope
      INNER JOIN tree ON tree.left = rope.id
        UNION
      SELECT rope.id, rope.left, rope.text, rope.right, rope.row_count, rope.last_col_count, tree.depth || '2'
      FROM rope
      INNER JOIN tree ON tree.right = rope.id
    )
    , full_tree AS (SELECT * FROM tree WHERE text IS NOT NULL ORDER BY depth)`


module.exports = createSQRope
