# SQRope

A Ropes data structure represented in SQL, with some helper functions
in Javascript to help insert, manipulate, and remove elements from it

## Ideas (not organized in any way)

- Ability to query to text in some range (returning not the _exact range_ but some
approximation based on what is on the SQL)
- Ability to edit without losing reference to what nodes where originally there
(the idea is - the editor should have "cells", representing text on the screen,
that are direct representations of the records on the SQL. When editing text
in one of these "cells", it will basically serve as a "hint" to what to update,
avoiding tree traversal)
- The ability to get the text but get the fragments of the text too, so the
editor knows which "cells" it needs to display
- Store more things on the SQL (like syntax highlight scopes) to avoid
re-triggering tokenization (and making tokens survive an undo/redo)

## But... Why?

SQL is powerful. The idea is to try to open the possibility of handling HUGE
files (4mb or more) without having to load them all in the memory, maintaining
some performance when editing.
