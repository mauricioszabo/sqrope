const {expect} = require('chai')
const createSQRope = require('../src/index')

describe('Rope data structure', () => {
  it('generates an empty node', async () => {
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
    await rope.loadString('')
    let result = await rope.queryAll('SELECT id, left, right, version, char_count FROM rope')
    expect(result).to.eql([{id: 1, left: null, right: null, version: 1, char_count: 0}])
  })

  it('generates a single node', async () => {
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
    await rope.loadString('as')
    let result = await rope.queryAll('SELECT id, left, right, version, char_count, text FROM rope')
    expect(result).to.eql([
      {id: 1, left: null, right: null, version: 1, char_count: 2, text: 'as'}
    ])
  })

  it('generates a balanced structure from a text', async () => {
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
    await rope.loadString('Hello, World!')

    let result = await rope.queryAll('SELECT id, left, right, version, char_count, text FROM rope')
    result = result.sort((a, b) => a.id - b.id)
    expect(result).to.eql([
      {id: 1, left: null, right: null, version: null, char_count: 3, text: "Hel"},
      {id: 2, left: null, right: null, version: null, char_count: 3, text: "lo,"},
      {id: 3, left: null, right: null, version: null, char_count: 3, text: " Wo"},
      {id: 4, left: null, right: null, version: null, char_count: 3, text: "rld"},
      {id: 5, left: null, right: null, version: null, char_count: 1, text: "!"},
      {id: 6, left: 1,    right: 2,    version: null, char_count: 6, text: null},
      {id: 7, left: 3,    right: 4,    version: null, char_count: 6, text: null},
      {id: 8, left: 6,    right: 7,    version: null, char_count: 12, text: null},
      {id: 9, left: 8,    right: 5,    version: 1,    char_count: 13, text: null},
    ]);
  })

  it('splits also in newlines', async () => {
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
    await rope.loadString("H\ne\nllo!")

    let result = await rope.queryAll(`SELECT char_count, row_count, text
      FROM rope WHERE text IS NOT NULL`)
    result = result.sort((a, b) => a.id - b.id)
    expect(result).to.eql([
      {row_count: 1, char_count: 3, text: "H\ne"},
      {row_count: 1, char_count: 3, text: "\nll"},
      {row_count: 0, char_count: 2, text: "o!"},
    ]);
  })

  it('gets the text of the tree on the current version', async () => {
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
    await rope.loadString('Hello, World!')
    expect(await rope.getText()).to.eql("Hello, World!")
  })

  describe('getting approximate ranges', () => {
    it.only('gets a node', async () => {
      let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
      await rope.loadString(`Hello
World Again


!!!
`)

      let node = await rope.getNodesInRange({
        start: {row: 1, column: 1},
        end: {row: 1, column: 4}
      })
      expect(node).to.eql({
        range: {
          start: {row: 1, column: 0},
          end: {row: 1, column: 6}
        }, records: [
          {id: 3, row_count: 0, last_col_count: 3, text: "Wor"},
          {id: 4, row_count: 0, last_col_count: 3, text: "ld "},
        ]
      })

      node = await rope.getNodesInRange({
        start: {row: 1, column: 3}, end: {row: 1, column: 5}
      })
      expect(node).to.eql({
        range: {
          start: {row: 1, column: 3},
          end: {row: 1, column: 6}
        }, records: [
          {id: 4, row_count: 0, last_col_count: 3, text: "ld "},
        ]
      })

      node = await rope.getNodesInRange({
        start: {row: 4, column: 0}, end: {row: 5, column: 0}
      })
      expect(node).to.eql({
        range: {
          start: {row: 2, column: 0},
          end: {row: 5, column: 0}
        },
        records: [
          {id: 7, row_count: 2, last_col_count: 1, text: "\n\n!"},
          {id: 8, row_count: 1, last_col_count: 0, text: "!!\n"},
        ]
      })
    })
  })

  describe('updating the structure', () => {
    it('updates nodes that are on different branches', async () => {
      // The key here is to test that we can update the X nodes:
      //         [ ]
      //        /   \
      //     [ ]     [ ]
      //    /  \     /  \
      //  [ ]  [X] [X]  [ ]

      let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
      await rope.loadString('Hello, world!')
      const {version, nodes} = await rope.update({nodeIds: [2, 3], newText: "p, A"})

      expect(await rope.getText()).to.eql('Help, Arld!')
      // Old version is still valid
      expect(await rope.getText(1)).to.eql('Hello, world!')

      // Check if nodes are correct
      expect(nodes.map(n => n.text)).to.eql(['p, ', 'A'])
      expect(await rope.queryAll('SELECT id,left,right FROM rope WHERE id > 9'))
        .to.eql([
          // Leaf nodes - "p, " and "A"
          {id: 10, left: null, right: null},
          {id: 11, left: null, right: null},
          // Branches - these share some of the original IDs.
          {id: 12, left: 1 , right: 10},
          {id: 13, left: 11, right: 4},
          {id: 14, left: 12, right: 13},
          {id: 15, left: 14, right: 5},
        ])
    })

    it('updates and inserts nodes', async () => {
      let rope = await createSQRope({db: ":memory:", rowChunkChars: 3})
      await rope.loadString('Hello, world!')
      const {version, nodes} = await rope.update({nodeIds: [2, 3], newText: "p, Apta"})

      expect(await rope.getText()).to.eql('Help, Aptarld!')
      // Old version is still valid
      expect(await rope.getText(1)).to.eql('Hello, world!')

      // Check if nodes are correct
      expect(nodes.map(n => n.text)).to.eql(['p, ', 'Apt', 'a'])
    })
  })

  xit('Load BIG file', async function() {
    this.timeout(50000)
    // const text = 'abc def ghi jkl\n'.repeat(1024 * 1024 * 20)
    const text = require('fs').readFileSync('/tmp/core2.txt', {encoding: 'utf-8'})
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 6000})
    console.time('Creation')
    await rope.loadString(text)
    console.timeEnd('Creation')

    console.time('get nodes in range')
    let node = await rope.getNodesInRange({
      start: {row: 4030, column: 1},
      end: {row: 4330, column: 0}
    })
    console.log(node.range, node.records.map(e => e.id))
    console.timeEnd('get nodes in range')

    console.time('edit')
    const {version, nodes} = await rope.update({nodeIds: [22], newText: "AB"})
    console.log("NODES", nodes)
    console.timeEnd('edit')
    // rope.getText()
  })

  xit('Benchmarks', async () => {
    const text = 'abc def ghi jkl\n'.repeat(1024 * 1024 * 4)
    let rope = await createSQRope({db: ":memory:", rowChunkChars: 6000})
    console.time('Creation')
    await rope.loadString(text)
    console.timeEnd('Creation')

    console.time('Get Text')
    expect(await rope.getText()).to.eql(text)
    console.timeEnd('Get Text')
  })
})

// function avoidDynamicFields(element) {
//   if(element instanceof Array) {
//     return element.map(avoidDynamicFields)
//   } else if(element instanceof Object) {
//     let newObj = {}
//     for(let k in element) {
//       if(k === 'id') continue;
//       if(k === 'left') continue;
//       if(k === 'right') continue;
//       if(k === 'ord') continue;
//       newObj[k] = avoidDynamicFields(element[k])
//     }
//     return newObj
//   } else {
//     return element
//   }
// }
